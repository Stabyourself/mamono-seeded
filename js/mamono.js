"use strict";
var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor)
        }
    }
    return function(Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);
        if (staticProps) defineProperties(Constructor, staticProps);
        return Constructor
    }
}();

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function")
    }
}
var CHIP_SIZE = 16;
var STATUS_HEIGHT = 16;
var canvas, ctx;
var bCanvas, bctx;
var rounds = [
    [-1, -1],
    [0, -1],
    [1, -1],
    [-1, 0],
    [1, 0],
    [-1, 1],
    [0, 1],
    [1, 1]
];
var images = {};
var loadImages = {
    chip: "images/chip.png",
    bignum: "images/bignum.png",
    smallnum: "images/smallnum.png",
    redsmallnum: "images/redsmallnum.png",
    greensmallnum: "images/greensmallnum.png",
    bigalpha: "images/bigalpha.png",
    smallalpha: "images/smallalpha.png"
};
var field;
var mamonoSize;
var panelSize;
var mamonoSizes;
var mamonoReveals;
var monsters;
var markingCounts;
var stringMap = {
    0: ["num", 0],
    1: ["num", 1],
    2: ["num", 2],
    3: ["num", 3],
    4: ["num", 4],
    5: ["num", 5],
    6: ["num", 6],
    7: ["num", 7],
    8: ["num", 8],
    9: ["num", 9],
    A: ["alpha", 0],
    B: ["alpha", 1],
    C: ["alpha", 2],
    D: ["alpha", 3],
    E: ["alpha", 4],
    F: ["alpha", 5],
    G: ["alpha", 6],
    H: ["alpha", 7],
    I: ["alpha", 8],
    J: ["alpha", 9],
    K: ["alpha", 10],
    L: ["alpha", 11],
    M: ["alpha", 12],
    N: ["alpha", 13],
    O: ["alpha", 14],
    P: ["alpha", 15],
    Q: ["alpha", 16],
    R: ["alpha", 17],
    S: ["alpha", 18],
    T: ["alpha", 19],
    U: ["alpha", 20],
    V: ["alpha", 21],
    W: ["alpha", 22],
    X: ["alpha", 23],
    Y: ["alpha", 24],
    Z: ["alpha", 25],
    ":": ["alpha", 26],
    "-": ["alpha", 27],
    x: ["alpha", 28],
    " ": ["alpha", 29]
};
var prevStatusLength = 0;
var prevTimeLength = 0;
var displayTime = 0;
var maxLevel
var Field = function() {
    function Field() {
        _classCallCheck(this, Field);
        this.width = fieldData.mapWidth;
        this.height = fieldData.mapHeight;
        this.expArray = fieldData.expArray;
        maxLevel = this.expArray.length+1;
        monsters = [];
        this.reset()
    }
    _createClass(Field, [{
        key: "drawAll",
        value: function drawAll() {
            for (var i = 0; i < this.height; i++) {
                for (var j = 0; j < this.width; j++) {
                    var chip = this.map[i][j];
                    chip.draw()
                }
            }
            var id = bctx.getImageData(0, 0, canvas.width, canvas.height);
            ctx.putImageData(id, 0, 0)
        }
    }, {
        key: "drawStatus",
        value: function drawStatus() {
            bctx.fillStyle = "#000000";
            bctx.fillRect(0, 0, prevStatusLength * 12, 16);
            var nxt;
            if (this.level < maxLevel) {
                var pnxt = this.level - 1;
                if (pnxt < 0) pnxt = 0;
                nxt = this.expArray[pnxt]
            } else {
                nxt = "-"
            }
            var txt = "LV:" + this.level + " HP:" + this.life + " EX:" + this.exp + " NE:" + nxt;
            drawText(bctx, txt, 0, -1, "big");
            prevStatusLength = txt.length;
            var step = 70;
            var width = step * mamonoSizes.length;
            var yy = STATUS_HEIGHT + this.height * CHIP_SIZE + 4;
            bctx.fillStyle = "#000000";
            bctx.fillRect(0, yy, canvas.width, yy + 16);
            for (var i = 0; i < mamonoSizes.length; i++) {
                var img = images.chip[fieldData.monsters[i].imagePlace];
                var xxx = canvas.width / 2 - width / 2 + step / 2 - CHIP_SIZE;
                if (mamonoReveals[i]) {
                    bctx.drawImage(img, xxx + i * step, yy)
                } else {
                    bctx.drawImage(images.chip[1], xxx + i * step, yy)
                }
                var txt = "LV" + (i + 1) + ":x" + mamonoSizes[i];
                drawText(bctx, txt, xxx + CHIP_SIZE + i * step, yy + 4)
            }
        }
    }, {
        key: "drawTime",
        value: function drawTime() {
            var _this = this;
            var loop = function loop() {
                if (_this.startTime == 0) return;
                if (_this.gameOver) return;
                displayTime = Math.floor(((new Date).getTime() - _this.startTime) / 1e3);
                var timeStr = displayTime.toString();
                if (displayTime != _this.prevTime) {
                    bctx.fillStyle = "#000000";
                    bctx.fillRect(canvas.width - prevTimeLength * 12, 0, prevTimeLength * 12, 16);
                    var txt = "TIME:" + timeStr;
                    drawText(bctx, txt, canvas.width, -1, "big", "right");
                    prevTimeLength = txt.length;
                    var id = bctx.getImageData(0, 0, canvas.width, STATUS_HEIGHT);
                    ctx.putImageData(id, 0, 0)
                }
                _this.prevTime = displayTime;
                requestAnimationFrame(loop)
            };
            loop()
        }
    }, {
        key: "open",
        value: function open(x, y, inner) {
            if (this.gameOver) return;
            if (x < 0 || x > this.width - 1 || y < 0 || y > this.height - 1) return;
            if (this.startTime == 0) {
                this.startTime = (new Date).getTime();
                this.drawTime();
                this.createMap(x, y)
            }
            var chip = this.map[y][x];
            if (chip.openFlag) {
                chip.showRound = !chip.showRound;
                chip.draw();
                return
            }
            if (chip.mark > this.level) return;
            chip.openFlag = true;
            chip.draw();
            var damage = false;
            var levelUp = false;
            if (chip.level > 0) {
                while (this.life > 0 && chip.life > 0) {
                    chip.life -= this.level;
                    if (chip.life <= 0) {
                        mamonoSize -= 1;
                        mamonoSizes[chip.level - 1] -= 1;
                        mamonoReveals[chip.level - 1] = true;
                        chip.life = 0;
                        this.exp += chip.exp;
                        if (this.exp >= this.expArray[this.level - 1]) {
                            this.level += 1;
                            levelUp = true
                        }
                        break
                    }
                    this.life -= chip.level;
                    damage = true;
                    if (this.life <= 0) {
                        this.life = 0
                    }
                }
                if (this.life == 0) this.doGameOver();
                this.drawStatus()
            } else {
                panelSize -= 1
            }
            if (chip.count == 0) {
                for (var i = 0; i < 8; i++) {
                    var xx = x + rounds[i][0];
                    var yy = y + rounds[i][1];
                    this.open(xx, yy, true)
                }
            }
            if (inner == true) return;
            if (fieldData.type == "battle" && mamonoSize == 0 || fieldData.type == "search" && panelSize == 0) {
                this.doClear()
            } else {
                if (damage) this.doDamage();
                if (levelUp) this.doLevelUp(damage)
            }
        }
    }, {
        key: "marking",
        value: function marking(x, y, num) {
            var isSearch = fieldData.type != "battle";
            if (this.gameOver) return;
            if (x < 0 || x > this.width - 1 || y < 0 || y > this.height - 1) return;
            var chip = this.map[y][x];
            if (isSearch && chip.mark > 0) mamonoSizes[chip.mark - 1] += 1;
            if (chip.openFlag) return;

            chip.mark = num

            if (isSearch && chip.mark > 0) mamonoSizes[chip.mark - 1] -= 1;
            if (isSearch) this.drawStatus();
            chip.draw()
        }
    }, {
        key: "doDamage",
        value: function doDamage() {
            ctx.fillStyle = "#000000";
            var id = bctx.getImageData(0, 0, canvas.width, canvas.height);
            var count = 0;
            var shake = function shake() {
                var rx = Math.random() * 20 - 10;
                var ry = Math.random() * 20 - 10;
                ctx.fillRect(0, 0, canvas.width, canvas.height);
                ctx.putImageData(id, rx, ry);
                if (count >= 10) {
                    shakeFinish();
                    return
                }
                count += 1;
                requestAnimationFrame(shake)
            };
            var shakeFinish = function shakeFinish() {
                var id = bctx.getImageData(0, 0, canvas.width, canvas.height);
                ctx.putImageData(id, 0, 0)
            };
            shake()
        }
    }, {
        key: "doLevelUp",
        value: function doLevelUp(damage) {
            var id = bctx.getImageData(0, 0, canvas.width, canvas.height);
            var alpha = 0;
            var plus = .1;
            ctx.fillStyle = "#00ff00";
            var co = 0;
            var bright = function bright() {
                if (damage && co <= 10) {
                    var rx = Math.random() * 20 - 10;
                    var ry = Math.random() * 20 - 10;
                    ctx.putImageData(id, rx, ry)
                } else {
                    ctx.putImageData(id, 0, 0)
                }
                alpha += plus;
                ctx.globalAlpha = alpha;
                ctx.fillRect(0, 0, canvas.width, canvas.height);
                if (alpha >= 1) plus = -plus;
                if (alpha <= 0) {
                    brightFinish();
                    return
                }
                co += 1;
                requestAnimationFrame(bright)
            };
            var brightFinish = function brightFinish() {
                ctx.globalAlpha = 1;
                var id = bctx.getImageData(0, 0, canvas.width, canvas.height);
                ctx.putImageData(id, 0, 0)
            };
            bright()
        }
    }, {
        key: "doGameOver",
        value: function doGameOver() {
            // CHANGE
            // DON'T REVEAL MONSTERS
            // for (var i = 0; i < this.height; i++) {
            //     for (var j = 0; j < this.width; j++) {
            //         var chip = this.map[i][j];
            //         if (chip.level > 0) {
            //             chip.openFlag = true;
            //             chip.draw()
            //         }
            //     }
            // }
            for (var i = 0; i < mamonoReveals.length; i++) {
                mamonoReveals[i] = true
            }
            this.drawStatus();
            this.gameOver = true
        }
    }, {
        key: "doClear",
        value: function doClear() {
            var _this2 = this;
            this.gameOver = true;
            this.clear = true;
            var id = ctx.getImageData(0, 0, canvas.width, STATUS_HEIGHT);
            for (var i = 0; i < monsters.length; i++) {
                var monster = monsters[i];
                monster.setRxRy()
            }
            var tweetText = document.querySelector("div#tweet_text");
            var jump = function jump() {
                if (!_this2.clear) return;
                ctx.fillStyle = "#000000";
                ctx.fillRect(0, STATUS_HEIGHT, canvas.width, fieldData.mapHeight * CHIP_SIZE);
                ctx.putImageData(id, 0, 0);
                for (var i = 0; i < monsters.length; i++) {
                    var monster = monsters[i];
                    monster.clearUpdate()
                }
                if (_this2.clear == false) return;
                requestAnimationFrame(jump)
            };
            var flash = function flash() {};
            jump();
            flash()
        }
    }, {
        key: "reset",
        value: function reset() {
            Math.seedrandom(seed);
            ctx.fillStyle = "#000000";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            bctx.fillStyle = "#000000";
            bctx.fillRect(0, 0, canvas.width, canvas.height);
            this.life = fieldData.life;
            this.level = fieldData.level;
            this.exp = fieldData.exp;
            this.gameOver = false;
            this.clear = false;
            monsters = [];
            this.createMamonoStatus();
            this.drawStatus();
            drawText(bctx, "TIME:0", canvas.width, -1, "big", "right");
            this.drawAll();
            var id = bctx.getImageData(0, 0, canvas.width, canvas.height);
            ctx.putImageData(id, 0, 0);
            mamonoSize = 0;
            for (var i = 0; i < fieldData.monsters.length; i++) {
                var monster = fieldData.monsters[i];
                mamonoSize += monster.quantity
            }
            panelSize = this.width * this.height - mamonoSize;
            this.startTime = 0;
            this.prevTime = 0;
            prevTimeLength = 6
        }
    }, {
        key: "createMamonoStatus",
        value: function createMamonoStatus() {
            var map = [];
            for (var i = 0; i < this.height; i++) {
                var line = [];
                for (var j = 0; j < this.width; j++) {
                    var chip = new Chip;
                    chip.x = j;
                    chip.y = i;
                    line.push(chip)
                }
                map.push(line)
            }
            mamonoSizes = [];
            mamonoReveals = [];
            for (var i = 0; i < fieldData.monsters.length; i++) {
                var data = fieldData.monsters[i];
                mamonoSizes.push(data.quantity);
                mamonoReveals.push(false)
            }
            this.map = map
        }
    }, {
        key: "createMap",
        value: function createMap(x, y) {
            var map = this.map;
            var stock = [];
            for (var i = 0; i < fieldData.monsters.length; i++) {
                var data = fieldData.monsters[i];
                for (var j = 0; j < data.quantity; j++) {
                    var chip = new Chip(data);
                    stock.push(chip);
                    monsters.push(chip)
                }
            }
            var points = [];
            for (var i = 0; i < this.height; i++) {
                for (var j = 0; j < this.width; j++) {
                    // CHANGE
                    // NO FIRST CLICK PROTECTION
                    // if (j == x && i == y) continue;
                    points.push([j, i])
                }
            }
            while (stock.length > 0) {
                var r = Math.floor(Math.random() * points.length);
                var point = points[r];
                points.splice(r, 1);
                var x = point[0];
                var y = point[1];
                var r2 = Math.floor(Math.random() * stock.length);
                var chip = stock[r2];
                stock.splice(r2, 1);
                map[y][x] = chip;
                chip.x = x;
                chip.y = y
            }
            for (var i = 0; i < this.height; i++) {
                for (var j = 0; j < this.width; j++) {
                    var sum = 0;
                    for (var k = 0; k < 8; k++) {
                        var xx = j + rounds[k][0];
                        var yy = i + rounds[k][1];
                        if (xx < 0 || xx > this.width - 1 || yy < 0 || yy > this.height - 1) {
                            continue
                        }
                        sum += map[yy][xx].level
                    }
                    map[i][j].count = sum
                }
            }
            return map
        }
    }]);
    return Field
}();
var Chip = function() {
    function Chip(data) {
        _classCallCheck(this, Chip);
        if (data != null) {
            this.name = data.name;
            this.level = data.level;
            this.life = this.level;
            this.exp = data.exp;
            this.image = images.chip[data.imagePlace];
            this.quantity = data.quantity
        } else {
            this.level = 0;
            this.image = images.chip[9]
        }
        this.openFlag = false;
        this.count = 0;
        this.x = this.y = 0;
        this.rx = this.ry = 0;
        this.rax = this.ray = 0;
        this.base = 0;
        this.mark = 0;
        this.showRound = false
    }
    _createClass(Chip, [{
        key: "draw",
        value: function draw() {
            var xx = this.x * CHIP_SIZE + (canvas.width - fieldData.mapWidth * CHIP_SIZE) / 2;
            var yy = this.y * CHIP_SIZE + STATUS_HEIGHT;
            if (this.openFlag) {
                bctx.drawImage(images.chip[9], xx, yy);
                if (this.level == 0 && this.count > 0) {
                    var txt = this.count.toString();
                    drawText(bctx, txt, xx + 8, yy + 3, "small", "center")
                } else {
                    if (this.level > 0 && this.showRound) {
                        var txt = this.count.toString();
                        drawText(bctx, txt, xx + 8, yy + 3, "small", "center", "red")
                    } else {
                        bctx.drawImage(this.image, xx, yy)
                    }
                }
            } else {
                bctx.drawImage(images.chip[0], xx, yy);
                if (this.mark > 0) {
                    var txt = this.mark.toString();
                    drawText(bctx, txt, xx + 8, yy + 3, "small", "center", "green")
                }
            }
        }
    }, {
        key: "setRxRy",
        value: function setRxRy() {
            this.rx = this.x * CHIP_SIZE + (canvas.width - fieldData.mapWidth * CHIP_SIZE) / 2;
            this.ry = this.y * CHIP_SIZE + STATUS_HEIGHT;
            this.rax = 0;
            this.ray = 0;
            this.base = STATUS_HEIGHT + fieldData.mapHeight * CHIP_SIZE - CHIP_SIZE
        }
    }, {
        key: "clearUpdate",
        value: function clearUpdate() {
            this.rx += this.rax;
            this.ry += this.ray;
            this.ray += .2;
            if (this.ry > this.base) {
                this.ry = this.base;
                this.rax = Math.random() * 6 - 3;
                this.ray = -Math.random() * 10
            }
            if (this.rx < 0) {
                this.rx = 0;
                this.rax = -this.rax
            }
            if (this.rx > canvas.width - CHIP_SIZE) {
                this.rx = canvas.width - CHIP_SIZE;
                this.rax = -this.rax
            }
            ctx.drawImage(this.image, this.rx, this.ry)
        }
    }]);
    return Chip
}();
window.addEventListener("load", function() {
    canvas = document.querySelector("canvas");
    // CHANGE
    // STOP TEXT SELECTION
    canvas.onselectstart = function () { return false; }
    ctx = canvas.getContext("2d");
    bCanvas = document.createElement("canvas");
    bCanvas.width = canvas.width;
    bCanvas.height = canvas.height;
    bctx = bCanvas.getContext("2d");
    for (var key in loadImages) {
        images[key] = new Image;
        images[key].src = loadImages[key]
    }
    ctx.fillStyle = "#ffffff";
    ctx.font = "16px sans-serif";
    ctx.fillText("Now Loading...", 0, 16);
    checkLoad()
});
var checkLoad = function checkLoad() {
    var co = 0;
    var total = 0;
    for (var key in images) {
        if (images[key].complete) co += 1;
        total += 1
    }
    if (co == total) {
        init();
        return
    }
    requestAnimationFrame(checkLoad)
};
var init = function init() {
    images.chip = createCropedImage(images.chip, CHIP_SIZE, CHIP_SIZE);
    images.bignum = createCropedImage(images.bignum, 12, 16);
    images.smallnum = createCropedImage(images.smallnum, 6, 8);
    images.redsmallnum = createCropedImage(images.redsmallnum, 6, 8);
    images.greensmallnum = createCropedImage(images.greensmallnum, 6, 8);
    images.bigalpha = createCropedImage(images.bigalpha, 12, 16);
    images.smallalpha = createCropedImage(images.smallalpha, 6, 8);
    var loop = function loop() {
        var flg = true;
        for (var key in images) {
            var imgs = images[key];
            for (var i in imgs) {
                if (imgs[i].width == 0) {
                    flg = false
                }
            }
        }
        if (flg) {
            firstLoad()
            return
        }
        requestAnimationFrame(loop)
    };
    loop();
    canvas.addEventListener("click", onClick);
    canvas.addEventListener("contextmenu", onRightClick)
    // CHANGE
    // ADD KEYDOWN EVENT
    document.addEventListener("keydown", onKeydown);
};
var onClick = function onClick(event) {
    if (field.gameOver) {
        field.reset()
    } else {
        var rect = canvas.getBoundingClientRect();
        var x = event.clientX - rect.left - (canvas.width - fieldData.mapWidth * CHIP_SIZE) / 2;
        var y = event.clientY - rect.top;
        var cx = Math.floor(x / CHIP_SIZE);
        var cy = Math.floor((y - STATUS_HEIGHT) / CHIP_SIZE);
        field.open(cx, cy);
        var id = bctx.getImageData(0, 0, canvas.width, canvas.height);
        ctx.putImageData(id, 0, 0)
    }
};
var onRightClick = function onRightClick(event) {
    var rect = canvas.getBoundingClientRect();
    var x = event.clientX - rect.left - (canvas.width - fieldData.mapWidth * CHIP_SIZE) / 2;
    var y = event.clientY - rect.top;
    var cx = Math.floor(x / CHIP_SIZE);
    var cy = Math.floor((y - STATUS_HEIGHT) / CHIP_SIZE);

    if (cx < 0 || cx > field.width - 1 || cy < 0 || cy > field.height - 1) return;

    var current = field.map[cy][cx].mark;
    var target = current + 1

    if (target > fieldData.monsters.length) {
        target = 0
    }
    field.marking(cx, cy, target);

    var id = bctx.getImageData(0, 0, canvas.width, canvas.height);
    ctx.putImageData(id, 0, 0);
    event.preventDefault()
};

// CHANGE
// MAKE A AND D MARK FIELDS
var mousePosition = {x:0, y:0};
document.addEventListener('mousemove', function(event){
    mousePosition.x = event.clientX;
    mousePosition.y = event.clientY;
}, false);

var onKeydown = function onKeydown(event) {
    var target;

    var rect = canvas.getBoundingClientRect();
    var x = mousePosition.x - rect.left - (canvas.width - fieldData.mapWidth * CHIP_SIZE) / 2;
    var y = mousePosition.y - rect.top;
    var cx = Math.floor(x / CHIP_SIZE);
    var cy = Math.floor((y - STATUS_HEIGHT) / CHIP_SIZE);

    if (cx < 0 || cx > field.width - 1 || cy < 0 || cy > field.height - 1) return;

    var current = field.map[cy][cx].mark;

    if (event.key == "a") {
        target = current - 1
        if (target < 0) {
            target = fieldData.monsters.length
        }
    } else if (event.key == "d") {
        target = current + 1
        if (target > fieldData.monsters.length) {
            target = 0
        }
    // } else if (!isNaN(event.key)) {
    //     target = parseInt(event.key)
    } else {
        return;
    }

    field.marking(cx, cy, target);
    var id = bctx.getImageData(0, 0, canvas.width, canvas.height);
    ctx.putImageData(id, 0, 0);
}
var drawText = function drawText(ctx, txt, x, y, size, align, color) {
    if (size == undefined) size = "small";
    if (align == undefined) align = "left";
    if (color == undefined) color = "";
    var width = 6;
    var height = 8;
    if (size == "big") {
        width = 12;
        height = 16
    }
    var xx = x;
    var yy = y;
    if (align == "right") {
        xx = x - width * txt.length
    } else if (align == "center") {
        xx = x - width * txt.length / 2
    }
    for (var i = 0; i < txt.length; i++) {
        var data = stringMap[txt.charAt(i)];
        var img = images[color + size + data[0]][data[1]];
        ctx.drawImage(img, xx + i * width, y)
    }
};
var createCropedImage = function createCropedImage(image, width, height) {
    var imgs = [];
    var lcanvas = document.createElement("canvas");
    lcanvas.width = image.width;
    lcanvas.height = image.height;
    var lctx = lcanvas.getContext("2d");
    var cropCanvas = document.createElement("canvas");
    cropCanvas.width = width;
    cropCanvas.height = height;
    var cctx = cropCanvas.getContext("2d");
    lctx.drawImage(image, 0, 0);
    for (var i = 0; i < image.height / height; i++) {
        for (var j = 0; j < image.width / width; j++) {
            cctx.putImageData(lctx.getImageData(j * width, i * height, width, height), 0, 0);
            var img = new Image;
            img.src = cropCanvas.toDataURL();
            imgs.push(img)
        }
    }
    return imgs
};