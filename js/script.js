var presets = [
    {
        name: "Mamono Sweeper on EASY",
        mapWidth: 16,
        mapHeight: 16,
        life: 10,
        level: 1,
        exp: 0,
        expArray: [7, 20, 50, 82],
        type: "battle",
        monsters: [10, 8, 6, 4, 2],
    },

    {
        name: "Mamono Sweeper on Normal",
        mapWidth: 30,
        mapHeight: 16,
        life: 10,
        level: 1,
        exp: 0,
        expArray: [10, 50, 167, 271],
        type: "battle",
        monsters: [33, 27, 20, 13, 6],
    },

    {
        name: "Mamono Sweeper on HUGE",
        mapWidth: 50,
        mapHeight: 25,
        life: 30,
        level: 1,
        exp: 0,
        expArray: [10,90,202,400,1072,1840,2992,4656],
        type: "battle",
        monsters: [52, 46, 40, 36, 30, 24, 18, 13, 1],
    },

    {
        name: "Mamono Sweeper on EXTREME",
        mapWidth: 30,
        mapHeight: 16,
        life: 10,
        level: 1,
        exp: 0,
        expArray: [10,50,175,375],
        type: "battle",
        monsters: [25, 25, 25, 25, 25],
    },

    {
        name: "Mamono Sweeper on BLIND",
        mapWidth: 30,
        mapHeight: 16,
        life: 1,
        level: 0,
        exp: 0,
        expArray: [9999, 9999, 9999, 9999],
        type: "search",
        monsters: [33, 27, 20, 13, 6],
    },

    {
        name: "Mamono Sweeper HUGE x EXTREME",
        mapWidth: 50,
        mapHeight: 25,
        life: 10,
        level: 1,
        exp: 0,
        expArray: [3, 10, 150, 540, 1116, 2268, 4572, 9180],
        type: "battle",
        monsters: [36, 36, 36, 36, 36, 36, 36, 36, 36],
    },

    {
        name: "Mamono Sweeper HUGE x BLIND",
        mapWidth: 50,
        mapHeight: 25,
        life: 1,
        level: 0,
        exp: 0,
        expArray: [9999,90,202,400,1072,1840,2992,4656],
        type: "search",
        monsters: [52, 46, 40, 36, 30, 24, 18, 13, 1],
    }
]

function makeSeed() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 16; i++)
        text += possible.charAt(Math.floor(realRandom() * possible.length));

    return text;
}

var query = window.location.search.substring(1);
var qs = parse_query_string(query);

var seed

if (qs.s != null) {
    seed = qs.s
} else {
    seed = makeSeed();
}

function applySeed() {
    seed = $("#seed").val()

    var tempArray = window.location.href.split("?");
    var baseURL = tempArray[0];
    window.history.replaceState('', '', baseURL + "?s=" + seed)

    $("#current-seed").html(seed)

    if (field != null) {
        parseOptions()
    }
}

function applyOptions(options) {
    fieldData = {
        mapWidth: options.mapWidth,
        mapHeight: options.mapHeight,
        life: options.life,
        level: options.level,
        exp: options.exp,

        expArray: options.expArray,
        type: options.type,
        monsters: []
    }

    for (var i = 0; i < options.monsters.length; i++) {
        fieldData.monsters[i] = {
            level: i+1,
            exp: Math.pow(2, i),
            imagePlace: i+2,
            quantity: options.monsters[i] || 0,
        }

        if (i > 4) {
            fieldData.monsters[i].imagePlace = i+7
        }

        if (i == 8) {
            fieldData.monsters[i].exp = 0
        }
    }

    // Resize canvas
    $("canvas")[0].width = Math.max(480, fieldData.mapWidth*16)
    $("canvas")[0].height = fieldData.mapHeight*16 + 40

    $("canvas")[0].getContext("2d").canvas.width = Math.max(480, fieldData.mapWidth*16)
    $("canvas")[0].getContext("2d").canvas.height = fieldData.mapHeight*16 + 40
    bCanvas.width = Math.max(480, fieldData.mapWidth*16)
    bCanvas.height = fieldData.mapHeight*16 + 40



    // Make old script stop
    if (field != null) {
        field.gameOver = true;
        field.clear = false;
    }
    field = new Field;
}

function parseOptions() {
    var options = {
        mapWidth: parseInt($("#mapWidth").val()),
        mapHeight: parseInt($("#mapHeight").val()),
        life: parseInt($("#life").val()),
        level: parseInt($("#level").val()),
        exp: parseInt($("#exp").val()),
        expArray: $("#expArray").val().split(","),
        monsters: $("#monsters").val().split(","),
        type: "battle",
    }

    if ($("#type-search").is(':checked')) {
        options.type = "search"
    }

    for (var i = 0; i < options.monsters.length; i++) {
        options.monsters[i] = parseInt(options.monsters[i].trim())
    }

    for (var i = 0; i < options.expArray.length; i++) {
        options.expArray[i] = parseInt(options.expArray[i].trim())
    }

    applyOptions(options)
}

function applyPreset(i) {
    $("#mapWidth").val(presets[i].mapWidth)
    $("#mapHeight").val(presets[i].mapHeight)
    $("#life").val(presets[i].life)
    $("#level").val(presets[i].level)
    $("#exp").val(presets[i].exp)
    $("#expArray").val(presets[i].expArray.join(", "))
    $("#monsters").val(presets[i].monsters.join(", "))

    if (presets[i].type == "battle") {
        $("#type-battle").prop("checked", true);
    } else {
        $("#type-search").prop("checked", true);
    }

    applyOptions(presets[i])
}

function firstLoad() {
    applyPreset(2)
}

function parse_query_string(query) {
    var vars = query.split("&");
    var query_string = {};
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split("=");
      var key = decodeURIComponent(pair[0]);
      var value = decodeURIComponent(pair[1]);
      // If first entry with this name
      if (typeof query_string[key] === "undefined") {
        query_string[key] = decodeURIComponent(value);
        // If second entry with this name
      } else if (typeof query_string[key] === "string") {
        var arr = [query_string[key], decodeURIComponent(value)];
        query_string[key] = arr;
        // If third or later entry with this name
      } else {
        query_string[key].push(decodeURIComponent(value));
      }
    }
    return query_string;
}

$(function() {
    $('[data-toggle="tooltip"]').tooltip()

    // Update the text field
    $("#seed").val(seed)

    applySeed(seed)

    // Bind the button
    $("#apply-seed").click(applySeed)

    // Bind return
    $('#seed').keypress(function (e) {
        if (e.which == 13) {
            applySeed()
            $(this).blur()
        }
    });

    // Bind apply options
    $("#apply-options").click(parseOptions)

    // Bind preset buttons
    $(".preset-button").click(function() {
        var presetI = parseInt($(this).data("i"))
        applyPreset(presetI)
    })

    // Bind random seed button
    $("#random-seed").click(function() {
        seed = makeSeed()
        $("#seed").val(seed)
        applySeed()
    })
})